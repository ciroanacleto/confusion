'use strict';
angular.module('conFusion.controllers', ['ionic.rating'])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $localStorage) {
"ngInject"

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    // Form data for the login modal
    $scope.loginData = $localStorage.getObject('userinfo', '{}');
    $scope.reservation = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });
    
    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/reserve.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.reserveForm = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
        $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function() {
        $scope.modal.show();
    };
    
    // Triggered in the reservation modal to close it
    $scope.closeReserve = function() {
        $scope.reserveForm.hide();
    };

    // Open the reservation modal
    $scope.reserve = function() {
        $scope.reserveForm.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
        console.log('Doing login', $scope.loginData);
        $localStorage.storeObject('userinfo', $scope.loginData);

        // Simulate a login delay. Remove this and replace with your login
        // code if using a login system
        $timeout(function() {
            $scope.closeLogin();
        }, 1000);
    };
    
    $scope.doReserve = function() {
        console.log('Doing reserve', $scope.reservation);

        $timeout(function() {
            $scope.closeReserve();
        }, 1000);
    };
})

.controller('MenuController', function ($scope, menuFactory, favoriteFactory, baseURL, $ionicListDelegate) {
"ngInject"

    $scope.baseURL = baseURL;
    $scope.tab = 1;
    $scope.filtText = '';
    $scope.showDetails = false;
    $scope.showMenu = false;
    $scope.message = "Loading ...";

    $scope.dishes = menuFactory.query(
        function(response) {
            $scope.dishes = response;
            $scope.showMenu = true;
        },
        function(response) {
            $scope.message = "Error: " + response.status + " " + response.statusText;
        });


    $scope.select = function(setTab) {
        $scope.tab = setTab;

        if (setTab === 2) {
            $scope.filtText = "appetizer";
        } else if (setTab === 3) {
            $scope.filtText = "mains";
        } else if (setTab === 4) {
            $scope.filtText = "dessert";
        } else {
            $scope.filtText = "";
        }
    };

    $scope.isSelected = function(checkTab) {
        return ($scope.tab === checkTab);
    };

    $scope.toggleDetails = function() {
        $scope.showDetails = !$scope.showDetails;
    };
    
    $scope.addFavorite = function (index) {
        console.log("index is " + index);
        favoriteFactory.addToFavorites(index);
        $ionicListDelegate.closeOptionButtons();
    }
})

.controller('ContactController', function($scope) {
"ngInject"

    $scope.feedback = {
        mychannel: "",
        firstName: "",
        lastName: "",
        agree: false,
        email: ""
    };

    var channels = [{
        value: "tel",
        label: "Tel."
    }, {
        value: "Email",
        label: "Email"
    }];

    $scope.channels = channels;
    $scope.invalidChannelSelection = false;

})

.controller('FeedbackController', function($scope, feedbackFactory) {
"ngInject"

    $scope.sendFeedback = function() {

        console.log($scope.feedback);

        if ($scope.feedback.agree && ($scope.feedback.mychannel == "")) {
            $scope.invalidChannelSelection = true;
            console.log('incorrect');
        } else {
            $scope.invalidChannelSelection = false;
            feedbackFactory.save($scope.feedback);
            $scope.feedback = {
                mychannel: "",
                firstName: "",
                lastName: "",
                agree: false,
                email: ""
            };
            $scope.feedback.mychannel = "";
            $scope.feedbackForm.$setPristine();
            console.log($scope.feedback);
        }
    };
})

.controller('DishDetailController', function($scope, $rootScope, $timeout, $stateParams, favoriteFactory, dish, menuFactory, baseURL, $ionicPopover, $ionicPopup) {
"ngInject"

        $scope.maxRating = 5;
        $scope.baseURL = baseURL;
        $scope.dish = {};
        $scope.showDish = false;
        $scope.message = "Loading ...";
        $scope.sorts = [{
            id: "1",
            key: "Date",
            value: "date"
        }, {
            id: "2",
            key: "Author",
            value: "author"
        }]

        $scope.dish = dish;

        $ionicPopover.fromTemplateUrl('templates/dishDetailPopover.html', {
            scope: $scope,
        }).then(function(popover) {
            $scope.popover = popover;
        });

        $scope.addFavorite = function (index) {
            favoriteFactory.addToFavorites(index);
            $scope.popover.hide();
        };
        
        $scope.newComment =  {
            rating: 0,
            comment: "",
            author: "",
        };

        $scope.addComment = function() {

            $scope.popover.hide();
            var popup = $ionicPopup.show({
                scope : $scope,
                title: 'Give us your comment!',
                templateUrl: 'templates/dishCommentPopup.html',
                buttons: [
                    { text: 'Close', onTap: function(e) { return false; } },
                    { text: 'Submit Comment', 
                      type: 'button-positive', 
                      onTap: function(e) { 
                          if(!$scope.newComment.rating || !$scope.newComment.comment || !$scope.newComment.author){
                              e.preventDefault();
                          } else {
                             return $scope.newComment; 
                          }
                        } 
                    }
                ]
                });
            popup.then(function(res) {
                if(res){
                    $scope.submitComment();
                    $timeout(function() {
                        popup.close();
                    }, 1000);
                }
            });
        };

        $scope.submitComment = function() {

            $scope.newComment.date = new Date().toISOString();
            
            var comment = angular.copy($scope.newComment);
            console.log(comment);

            $scope.dish.comments.push(comment);
            menuFactory.getDishes().update({
                id: $scope.dish.id
            }, $scope.dish);

            $scope.newComment = {
                rating: 0,
                comment: "",
                author: "",
                date: ""
            };
        }
    })

.controller('DishCommentController', function($scope, $rootScope, menuFactory) {
"ngInject"

    $scope.mycomment = {
        rating: 5,
        comment: "",
        author: "",
        date: ""
    };

    $scope.submitComment = function() {

        $scope.mycomment.date = new Date().toISOString();
        console.log($scope.mycomment);

        $scope.dish.comments.push($scope.mycomment);
        menuFactory.getDishes().update({
            id: $scope.dish.id
        }, $scope.dish);

        $scope.commentForm.$setPristine();

        $scope.mycomment = {
            rating: 5,
            comment: "",
            author: "",
            date: ""
        };
    }
})

.controller('IndexController', function($scope, menuFactory, promotionFactory, corporateFactory, baseURL) {
"ngInject"

    $scope.baseURL = baseURL;
    $scope.leader = corporateFactory.get({
        id: 3
    });
    $scope.showDish = false;
    $scope.message = "Loading ...";
    $scope.dish = menuFactory.get({
            id: 0
        })
        .$promise.then(
            function(response) {
                $scope.dish = response;
                $scope.showDish = true;
            },
            function(response) {
                $scope.message = "Error: " + response.status + " " + response.statusText;
            }
        );
    $scope.promotion = promotionFactory.get({
        id: 0
    });

})

.controller('AboutController', function($scope, corporateFactory) {
"ngInject"

    $scope.leaders = corporateFactory.query();
    console.log($scope.leaders);

})

.controller('FavoritesController', function ($scope, dishes, favorites, favoriteFactory, baseURL, $ionicListDelegate, $ionicPopup, 
    $ionicLoading, $timeout) {
"ngInject"

    $scope.baseURL = baseURL;
    $scope.shouldShowDelete = false;

    $scope.favorites = favorites;

    $scope.dishes = dishes;

    console.log($scope.dishes, $scope.favorites);

    $scope.toggleDelete = function () {
        $scope.shouldShowDelete = !$scope.shouldShowDelete;
        console.log($scope.shouldShowDelete);
    }

    $scope.deleteFavorite = function (index) {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Confirm Delete',
            template: 'Are you sure you want to delete this item ?'
        });

        confirmPopup.then(function(res){
            if(res){
                console.log('Ok to delete');
                favoriteFactory.deleteFromFavorites(index);
            } else {
                console.log('Canceled delete');
            }
        })
        $scope.shouldShowDelete = false;

    }
})

.filter('favoriteFilter', function () {
    return function (dishes, favorites) {
        var out = [];
        for (var i = 0; i < favorites.length; i++) {
            for (var j = 0; j < dishes.length; j++) {
                if (dishes[j].id === favorites[i].id)
                    out.push(dishes[j]);
            }
        }
        return out;
    }
});